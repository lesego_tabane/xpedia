package za.co.xpedia.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAutoConfiguration
@EnableScheduling
public class DirectorySyncApplication {

	public static void main(String[] args) {
		SpringApplication.run(DirectorySyncApplication.class, args);
	}

}
