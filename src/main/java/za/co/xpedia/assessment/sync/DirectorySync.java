package za.co.xpedia.assessment.sync;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.SimpleFileVisitor;

@Component
public class DirectorySync implements SyncInterface{
	
	@Value("${sync.source}")
	private String sourceLocation;
	@Value("${sync.target}")
	private String targetLocation;
	@Value("${sync.depth}")
	private int folderDepth;
	
	private Path source;
	private Path target;
	
	private EnumSet<FileVisitOption> fileVisitOptions = EnumSet.of(FileVisitOption.FOLLOW_LINKS);

	public DirectorySync() {
	}
	
	@Override
	public void syncDirectories() {
  		System.out.println("####### Running Sync");
		source = Paths.get(sourceLocation);
		target = Paths.get(targetLocation);
		syncSourceToTarget(source, target);
		removeExtraTargetFiles(source, target);
	}
	
	public void syncSourceToTarget(Path source, Path target) {
		
			try {
				Files.walkFileTree(source, fileVisitOptions, folderDepth, new SimpleFileVisitor<Path>() {

					
					
					@Override
					public FileVisitResult preVisitDirectory(Path file, BasicFileAttributes arg1) throws IOException {
						Path fileInSource = (source.relativize(file));
						Path fileInTarget = target.resolve(fileInSource);
						
				        if(Files.notExists(fileInTarget)) {
				        	try {
				        		System.out.println("Adding new directory : "+fileInTarget.toString());
								Files.createDirectories(fileInTarget);
								
							} catch (IOException e) {
								System.out.println("Directory Create Error: "+e.getMessage());
							}
				        }
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)  {
						
						Path fileInSource = (source.relativize(file));
						Path fileInTarget = target.resolve(fileInSource);
						
				        if(Files.notExists(fileInTarget)) {
				        	try {
				        		System.out.println("Adding new file : "+fileInTarget.toString());
								if(Files.isDirectory(fileInTarget)) {
									Files.createDirectories(fileInTarget);
								}else {
									Files.createFile(fileInTarget);
								}
							} catch (IOException e) {
								System.out.println("File Copy Error: "+e.getMessage());
							}
				        }
						return FileVisitResult.CONTINUE;
					}
				});
			} catch (IOException e) {
				System.out.println("File Walk Error: "+e.getMessage());
			}
		
	}
	
	public void removeExtraTargetFiles(Path source, Path target) {
		try {
			Files.walkFileTree(target, fileVisitOptions, folderDepth, new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					
					Path fileInTarget = file;
			        Path fileInSource = source.resolve(target.relativize(file));
			        
			        if(Files.exists(fileInSource) && !Files.isDirectory(fileInTarget) ) {
			        	if(!(Files.getLastModifiedTime(fileInSource).equals(Files.getLastModifiedTime(fileInTarget))
			        			&& Files.size(fileInTarget)==Files.size(fileInSource))) {
			        		
			        		try{
			        			System.out.println("*** Replacing: "+ fileInTarget+" with "+ (fileInSource));
			        			Files.copy(fileInSource, fileInTarget, StandardCopyOption.REPLACE_EXISTING);
			        		}catch (Exception e) {
			        			System.out.println("Error replacing file: "+e.getMessage());
							}
			        		
			        	}
			        }else {
			        	try {
			        		System.out.println("Deleting File: "+ fileInTarget);
			        		Files.delete(fileInTarget);
			        		
			        	}catch (IOException e) {
			        		System.out.println("Error Deleting File: "+e.getMessage());
						}
			        }
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					Path fileInTarget = dir;
			        Path fileInSource = source.resolve(target.relativize(dir));
			        
			        if(Files.notExists(fileInSource) ) {
			        	
			        	try {
			        		System.out.println("Deleting File: "+ fileInTarget);
			        		Files.delete(fileInTarget);
			        		
			        	}catch (IOException e) {
			        		System.out.println("Error Deleting File: "+e.getMessage());
						}
			        }
					return FileVisitResult.CONTINUE;
				}
				
				
				
			});
		} catch (IOException e) {
			System.out.println("Error: "+e.getMessage());
			e.printStackTrace();
		}
		
		
	}

	public String getSourceLocation() {
		return sourceLocation;
	}

	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public String getTargetLocation() {
		return targetLocation;
	}

	public void setTargetLocation(String targetLocation) {
		this.targetLocation = targetLocation;
	}

	public int getFolderDepth() {
		return folderDepth;
	}

	public void setFolderDepth(int folderDepth) {
		this.folderDepth = folderDepth;
	}

}
