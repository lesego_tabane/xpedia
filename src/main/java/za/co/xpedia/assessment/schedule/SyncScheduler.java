package za.co.xpedia.assessment.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import za.co.xpedia.assessment.sync.DirectorySync;
import za.co.xpedia.assessment.sync.SyncInterface;

@Component
public class SyncScheduler {
	
	@Autowired
	private SyncInterface directorySync;

	@Scheduled(fixedRateString="${schedule.interval}")
	public void scheduleTaskWithFixedRate() {
		directorySync.syncDirectories();
	}
}
